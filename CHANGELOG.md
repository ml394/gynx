# Changelog
All notable changes to the **gynx** project will be documented in this file.


## [0.0.8] - 2020-03-24
### Added
- CI container and security scanning

### Changed
- Fix CI runtime errors

### Removed
- Google API logging


## [0.0.7] - 2019-11-27
### Added
- Docstrings and code comments
- Logging settings and custom logging function

### Changed
- Various bug fixes
- Full code cleanup

### Removed
- Unused class methods and associated tests


## [0.0.6] - 2019-11-25
### Added
- Additional tests and code coverage

### Changed
- Fixed argument parsing error

### Removed
- N/A


## [0.0.5] - 2019-11-25
### Added
- Positional argument to start automatic folder monitoring
- Custom gynx event class handled by the event handler
- Tests for the above

### Changed
- Upload and download operations functions

### Removed
- N/A


## [0.0.4] - 2019-11-21
### Added
- watchdog library for real time monitoring of local file changes
- schedule library for scheduled sync operations

### Changed
- The way arguments are parsed and handled. This is now done in `gynx_run.py`
- Main app can optionally be run on a schedule of custom duration

### Removed
- Partial Python 2 support


## [0.0.3] - 2019-04-28
### Added
- Issue fixes

### Changed
- Use parent in remote create folder function

### Removed
- N/A


## [0.0.2] - 2019-04-09
### Added
- Full pytest test suite with 90% code coverage
- Functioning Gitlab CI pipelines (build, test, deploy)
- Environment-specific settings module

### Changed
- Configuration variables based on environment (see *Added*)
- Python gynx_run.py script name

### Removed
- Unused files changes module (under development)


## [0.0.1] - 2019-02-15
### Added
- Core gynx package and app structure
- Core files modules: readers, differences, operations
- Root app workflow
- Run scripts
- Base app credentials
- PyPI package setup and installation

### Changed
- N/A

### Removed
- N/A


## [Unreleased] - 2018-12-06
- Initial project commit
