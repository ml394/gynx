## Contribute to gynx

Thanks for your interest in contributing to the **gynx** project.
Take a look at the following steps to get started.

We assume you are working on a machine with a standardized Linux filesystem hierarchy,
and that you have a working Python 2/3 virtual environment set up and activated.

1. Clone the `development` branch and install requirements.
```
git clone -b development https://gitlab.com/ml394/gynx.git
pip install -r requirements.txt
```
2. Create your own feature/fix branch.
```
git checkout -b <your-branch-name>
```
3. Commit your changes and push your branch.
```
git add -A
git commit -m "This is what I changed"
git push --set-upstream origin <your-branch-name>
```
If your commit provides a fix for a listed issue, prepend your final commit message with `Fix/Resolve/Close #N` (where `N` is the issue number), to automatically close the issue after the approved merge. For example:
```
git commit -m "Fix #99 - error handling in file reader get method"
```
4. Create a merge request from your branch to `development` by visiting the [Merge Requests](https://gitlab.com/ml394/gynx/merge_requests) page.
5. Wait for your code to be reviewed. Keep an eye on the comment thread.
6. If your merge request is accepted, it will be merged into the staging and master branches for the next full release.

### Testing

This project is most definitely still in an *experimental phase* and therefore *should not* be used to sync any important Google Drive files at this stage. The simplest way to test is to create a new Google account for testing purposes, or use an account without any personal Google Drive data currently stored in the cloud.

Once you have a working Google Drive account for testing, clone the repo and add the cloned repository's `bin/` folder to `PATH` by adding this line to your `~/.bashrc`. This will allow you to use the gynx commands in the repository.
```
export PATH=$PATH:/path/to/repo/bin
```
**Please note** this will not work if **gynx** is already installed in your home folder or virtual environment. Verify that `which gynx` is blank before adding the `bin/` folder to `PATH`.
