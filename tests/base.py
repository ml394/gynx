from unittest import TestCase
import os
import dictdiffer
from gynx.files.readers import RemoteFileReader
from gynx.files.differences import Difference, Differences
from gynx.files.operations import SyncOperations
from googleapiclient.errors import HttpError

APP_DIRECTORY = os.path.join(os.getcwd(), 'gynx/')
DRIVE_DIRECTORY = os.path.join(os.getcwd(), 'tests', 'drive/')

def setup_folders():
    '''
    Setup cache folder and files, and test drive directory if they don't exist
    '''
    cache = os.path.join(APP_DIRECTORY, 'CACHE')
    if not os.path.exists(cache):
        os.mkdir(cache)
    remote = os.path.join(cache, 'remote.json')
    if os.path.exists(remote):
        os.rename(remote, remote + '.tmp')
    local = os.path.join(cache, 'local.json')
    if os.path.exists(local):
        os.rename(local, local + '.tmp')

    if not os.path.exists(DRIVE_DIRECTORY):
        os.mkdir(DRIVE_DIRECTORY)

def delete_test_files(directory):
    '''
    Delete all files in local test directory
    '''
    for item in os.listdir(directory):
        path = os.path.join(directory, item)
        try:
            os.remove(path)
        except OSError:
            try:
                os.rmdir(path)
            except OSError:
                delete_test_files(path)

def delete_remote_files(service, remote):
    '''
    Attempt to delete all files in remote test directory
    '''
    for name in remote.get('drive', []):
        f = remote['drive'][name]
        if f.get('child_id'):
            try:
                service.files().delete(fileId=f['child_id']).execute()
            except HttpError:
                print(u'Remote file not found: %s' % (f['child_id']))
            try:
                service.files().delete(fileId=f.get('parent_id', '')).execute()
            except HttpError or KeyError:
                print(u'Remote folder not found: %s' % (str(f)))

def cleanup_folders():
    '''
    Remove cache and delete test files
    '''
    cache = os.path.join(APP_DIRECTORY, 'CACHE')
    remote_tmp = os.path.join(cache, 'remote.json.tmp')
    remote = os.path.join(cache, 'remote.json')
    if os.path.exists(remote):
        os.remove(remote)
    if os.path.exists(remote_tmp):
        os.rename(remote_tmp, remote)
    local_tmp = os.path.join(cache, 'local.json.tmp')
    local = os.path.join(cache, 'local.json')
    if os.path.exists(local):
        os.remove(local)
    if os.path.exists(local_tmp):
        os.rename(local_tmp, local)
    delete_test_files(DRIVE_DIRECTORY)

def create_local_file(filename):
    '''
    Create a local file in the test drive directory
    '''
    filepath = os.path.join(DRIVE_DIRECTORY, filename)
    with open(filepath, 'w+') as f:
        f.write('Test File %s' % filename)
    return filepath

def remove_local_file(filename):
    '''
    Remove a local file in the test drive directory
    '''
    filepath = os.path.join(DRIVE_DIRECTORY, filename)
    os.remove(filepath)

def create_remote_file(service, path):
    '''
    Upload a test file to the remote drive directory
    '''
    operations = SyncOperations(service=service, changes=None)
    operations.upload(
        mimetype='text/plain',
        local_path=path
    )

def read_remote(gynx):
    '''
    Read the contents of the remote drive directory
    '''
    remote_reader = RemoteFileReader(
        src=gynx.appdir,
        service=gynx.service,
        info=gynx.get_info(),
        quiet=False
    )
    return remote_reader.files

## Test Data ##
class Data:
    '''
    Class containing all relevant test data
    '''
    # Standard Dictionaries
    DICT_A = {'a': {'b': {'c': 1}}}
    DICT_B = {'a': {'b': {'c': 2}}}
    DICT_C = {'a': {'b': {'c': 2, 'd': 3}}}
    DICT_D = {'a': {'b': {'c': 2}}}
    ALL_DICTS = [DICT_A, DICT_B, DICT_C, DICT_D]
    # Differences
    DIFF_AB = [Difference(d) for d in list(dictdiffer.diff(DICT_A, DICT_B))]
    DIFF_BA = [Difference(d) for d in list(dictdiffer.diff(DICT_B, DICT_A))]
    DIFF_BC = [Difference(d) for d in list(dictdiffer.diff(DICT_B, DICT_C))]
    DIFF_CB = [Difference(d) for d in list(dictdiffer.diff(DICT_C, DICT_B))]
    DIFF_CD = [Difference(d) for d in list(dictdiffer.diff(DICT_C, DICT_D))]
    DIFF_DC = [Difference(d) for d in list(dictdiffer.diff(DICT_D, DICT_C))]
    ALL_DIFFS = [DIFF_AB, DIFF_BA, DIFF_BC, DIFF_CB, DIFF_CD, DIFF_DC]
    DIFFERENCES_AB = Differences(remote_files=DICT_A, local_files=DICT_B)
    DIFFERENCES_BC = Differences(remote_files=DICT_B, local_files=DICT_C)
    DIFFERENCES_CD = Differences(remote_files=DICT_C, local_files=DICT_D)
    ALL_DIFFERENCES = [DIFFERENCES_AB, DIFFERENCES_BC, DIFFERENCES_CD]
    DIFF_BROKEN_TYPE = Difference(dd=['badtype', [1, 2, 3]])
    DIFF_BROKEN_PATH = Difference(dd=['add', 0])

    # Folder/File Dictionaries
    FILES_A = {'drive': {'docs': {'file.txt': {
        'child_id': 1,
        'parent_id': 2,
        'extension': 'TXT',
        'mimetype': 'text/plain',
        'modified': 'Sat Feb 12 01:10:39 2019',
        'path': '/docs/file.txt',
        'size': 5
    }}}}
    FILES_B = {'drive': {'docs': {'file.txt': {
        'extension': 'TXT',
        'mimetype': 'text/plain',
        'modified': 'Sat Feb 19 01:10:39 2019',
        'path': '/docs/file.txt',
        'size': 6
    }}}}
    FILES_C = {'drive': {'docs': {'file.txt': {
        'extension': 'TXT',
        'mimetype': 'text/plain',
        'modified': 'Sat Feb 05 01:10:39 2019',
        'path': '/docs/file.txt',
        'size': 4
    }}}}
    FILES_D = {'drive': {'docs': {'file.txt': {
        'extension': 'TXT',
        'mimetype': 'text/plain',
        'modified': 'Sat Feb 12 01:10:39 2019',
        'path': '/docs/file.txt',
        'size': 5
    }}}}
    FILES_E = {'drive': {'docs': {
        'file.txt': {
            'extension': 'TXT',
            'mimetype': 'text/plain',
            'modified': 'Sat Feb 12 01:10:39 2019',
            'path': '/docs/file.txt',
            'size': 5
        },
        'image.jpg': {
            'extension': 'JPG',
            'mimetype': 'image/jpeg',
            'modified': 'Sat Feb 18 01:10:39 2019',
            'path': '/docs/image.jpg',
            'size': 50
        }
    }}}
    FILES_F = {'drive': {'docs': {}}}
    FILES_G = {'drive': {'docs': {
        'file.txt': {
            'child_id': 1,
            'parent_id': 2,
            'extension': 'TXT',
            'mimetype': 'text/plain',
            'modified': 'Sat Feb 12 01:10:39 2019',
            'path': '/docs/file.txt',
            'size': 5
        },
        'image.jpg': {
            'child_id': 3,
            'parent_id': 2,
            'extension': 'JPG',
            'mimetype': 'image/jpeg',
            'modified': 'Sat Feb 18 01:10:39 2019',
            'path': '/docs/image.jpg',
            'size': 50
        }
    }}}
    FILES_H = {'drive': {'docs': {}}}
    FILES_I = {'drive': {'docs': {'renamed.txt': {
        'child_id': 1,
        'parent_id': 2,
        'extension': 'TXT',
        'mimetype': 'text/plain',
        'modified': 'Sat Feb 12 01:10:39 2019',
        'path': '/docs/files.txt',
        'size': 5
    }}}}
    FILES_J = {'drive': {'docs': {'renamed.txt': {
        'extension': 'TXT',
        'mimetype': 'text/plain',
        'modified': 'Sat Feb 12 01:10:39 2019',
        'path': '/docs/renamed.txt',
        'size': 5
    }}}}
    FILES_K = {'drive': {'docs': {
        'file.txt': {
            'extension': 'TXT',
            'mimetype': 'text/plain',
            'modified': 'Sat Feb 12 01:10:39 2019',
            'path': '/docs/file.txt',
            'size': 5
        },
        'folder': {}
    }}}
    FILES_L = {'drive': {'docs': {
        'file.txt': {
            'child_id': 1,
            'parent_id': 2,
            'extension': 'TXT',
            'mimetype': 'text/plain',
            'modified': 'Sat Feb 12 01:10:39 2019',
            'path': '/docs/file.txt',
            'size': 5
        },
        'folder': {}
    }}}
    ALL_FILES = [
        FILES_A, FILES_B, FILES_C, FILES_D, FILES_E, FILES_F, FILES_G, FILES_H
    ]
    # Files Differences
    FILES_DIFF_AB = [
        Difference(d) for d in list(dictdiffer.diff(FILES_A, FILES_B))
    ]
    FILES_DIFF_BA = [
        Difference(d) for d in list(dictdiffer.diff(FILES_B, FILES_A))
    ]
    FILES_DIFF_AC = [
        Difference(d) for d in list(dictdiffer.diff(FILES_A, FILES_C))
    ]
    FILES_DIFF_CA = [
        Difference(d) for d in list(dictdiffer.diff(FILES_C, FILES_A))
    ]
    FILES_DIFF_AD = [
        Difference(d) for d in list(dictdiffer.diff(FILES_A, FILES_D))
    ]
    FILES_DIFF_DA = [
        Difference(d) for d in list(dictdiffer.diff(FILES_D, FILES_A))
    ]
    FILES_DIFF_AE = [
        Difference(d) for d in list(dictdiffer.diff(FILES_A, FILES_E))
    ]
    FILES_DIFF_EA = [
        Difference(d) for d in list(dictdiffer.diff(FILES_E, FILES_A))
    ]
    FILES_DIFF_AF = [
        Difference(d) for d in list(dictdiffer.diff(FILES_A, FILES_F))
    ]
    FILES_DIFF_FA = [
        Difference(d) for d in list(dictdiffer.diff(FILES_F, FILES_A))
    ]
    FILES_DIFF_GD = [
        Difference(d) for d in list(dictdiffer.diff(FILES_G, FILES_D))
    ]
    FILES_DIFF_DG = [
        Difference(d) for d in list(dictdiffer.diff(FILES_D, FILES_G))
    ]
    FILES_DIFF_HD = [
        Difference(d) for d in list(dictdiffer.diff(FILES_H, FILES_D))
    ]
    FILES_DIFF_DH = [
        Difference(d) for d in list(dictdiffer.diff(FILES_D, FILES_H))
    ]
    ALL_FILES_DIFFS = [
        FILES_DIFF_AB, FILES_DIFF_BA, FILES_DIFF_AC, FILES_DIFF_CA,
        FILES_DIFF_AD, FILES_DIFF_DA, FILES_DIFF_AE, FILES_DIFF_EA,
        FILES_DIFF_AF, FILES_DIFF_FA, FILES_DIFF_GD, FILES_DIFF_DG,
        FILES_DIFF_HD, FILES_DIFF_DH
    ]
    FILES_DIFFERENCES_AB = Differences(
        FILES_A, FILES_B, initial=True, root='/home/gynx/'
    )
    FILES_DIFFERENCES_AC = Differences(
        FILES_A, FILES_C, initial=True, root='/home/gynx/'
    )
    FILES_DIFFERENCES_AD = Differences(
        FILES_A, FILES_D, initial=True, root='/home/gynx/'
    )
    FILES_DIFFERENCES_AE = Differences(
        FILES_A, FILES_E, initial=True, root='/home/gynx/'
    )
    FILES_DIFFERENCES_AF = Differences(
        FILES_A, FILES_F, root='/home/gynx/'
    )
    FILES_DIFFERENCES_GD = Differences(
        FILES_G, FILES_D, initial=True, root='/home/gynx/'
    )
    FILES_DIFFERENCES_HD = Differences(
        FILES_H, FILES_D, previous=FILES_A, root='/home/gynx/'
    )
    FILES_DIFFERENCES_AJ = Differences(
        FILES_A, FILES_J, previous=FILES_A, root='/home/gynx/'
    )
    FILES_DIFFERENCES_ID = Differences(
        FILES_I, FILES_D, previous=FILES_A, root='/home/gynx/'
    )
    FILES_DIFFERENCES_AK = Differences(
        FILES_A, FILES_K, previous=FILES_A, root='/home/gynx/'
    )
    FILES_DIFFERENCES_LD = Differences(
        FILES_L, FILES_D, previous=FILES_A, root='/home/gynx/'
    )
    ALL_FILES_DIFFERENCES = [
        FILES_DIFFERENCES_AB, FILES_DIFFERENCES_AC, FILES_DIFFERENCES_AD,
        FILES_DIFFERENCES_AE, FILES_DIFFERENCES_AF,
        # FILES_DIFFERENCES_GD, FILES_DIFFERENCES_HD
    ]

    # Merged Changes
    CHANGES = [
        {
            'type': 'change',
            'folder': False,
            'local_path': '/home/gynx/docs/file.txt',
            'remote_id': 1,
            'target': 'remote',
            'message': 'docs/file.txt updated in local. Uploading...'
        },
        {
            'type': 'change',
            'folder': False,
            'local_path': '/home/gynx/docs/file.txt',
            'remote_id': 1,
            'target': 'local',
            'message': 'docs/file.txt updated in remote. Downloading...'
        },
        {
            'folder': False,
            'local_path': '/home/gynx/docs/image.jpg',
            'remote_id': None,
            'type': 'add',
            'target': 'remote',
            'message': 'docs/image.jpg created in local. Uploading...'
        },
        {
            'folder': False,
            'local_path': '/home/gynx/docs/file.txt',
            'remote_id': 1,
            'type': 'remove',
            'target': 'remote',
            'message': 'docs/file.txt removed in local. Deleting remote...'
        },
        {
            'folder': True,
            'local_path': '/home/gynx/docs/folder/',
            'remote_id': None,
            'type': 'add',
            'target': 'remote',
            'message': 'docs/folder created in local. Creating remote folder...'
        },
        {
            'folder': True,
            'local_path': '/home/gynx/docs/folder/',
            'remote_id': 2,
            'type': 'add',
            'target': 'local',
            'message': 'docs/folder created in remote. Creating local folder...'
        },
        {
            'folder': False,
            'local_path': '/home/gynx/docs/image.jpg',
            'remote_id': None,
            'type': 'remove',
            'target': 'local',
            'message': 'docs/image.jpg removed in remote. Deleting local...'
        },
        {
            'folder': False,
            'local_path': '/home/gynx/docs/image.jpg',
            'remote_id': None,
            'type': 'rename',
            'target': 'remote',
            'message': 'docs/image.jpg renamed in local. Renaming remote...'
        },
        {
            'folder': False,
            'local_path': '/home/gynx/docs/image.jpg',
            'remote_id': 1,
            'type': 'rename',
            'target': 'local',
            'message': 'docs/image.jpg renamed in remote. Renaming local...'
        }
    ]

def difference_equal(diff_a, diff_b):
    '''
    Assert whether two difference objects are EXACTLY equal
    '''
    assert(diff_a.dd == diff_b.dd)
    assert(diff_a.type == diff_b.type)
    assert(diff_a.path == diff_b.path)
    assert(diff_a.key == diff_b.key)
    assert(diff_a.target == diff_b.target)
    assert(diff_a.remote_modified == diff_b.remote_modified)
    assert(diff_a.local_modified == diff_b.local_modified)

def differences_equal(diffs_a, diffs_b):
    '''
    Assert whether two sets of difference objects are EXACTLY equal
    '''
    for i in range(2):
        for diff_a in diffs_a[i]:
            for diff_b in diffs_b[i]:
                difference_equal(diff_a, diff_b)
    return True

def files_differences_equal(file_diffs_a, file_diffs_b):
    '''
    Assert whether two sets of file differences are equal
    '''
    for i in range(2):
        for fda in file_diffs_a[i]:
            found = False
            for fdb in file_diffs_b[i]:
                try:
                    difference_equal(fda, fdb)
                    found = True
                except AssertionError:
                    continue
            assert(found)
    return True
