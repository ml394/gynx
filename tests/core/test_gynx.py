''' Tests fore gynx.core '''

from tests.base import *
from gynx.core import Gynx, log
import logging

def test_log():
    log(message='Test Message 1')
    log(message='Test Message 2', method=logging.info)


class TestGynx(TestCase):

    def setUp(self):
        self.gynx = Gynx()

    def test_service(self):
        pass

    def test_str(self):
        self.assertEqual(
            str(self.gynx),
            'gynx initialized at %s' % str(self.gynx.start)
        )

    def test_get_credentials(self):
        try:
            self.gynx.get_credentials()
            authorized = True
        except:
            authorized = False
        self.assertTrue(authorized)

    def test_get_info(self):
        info = self.gynx.get_info()
        self.assertIn('user', info.keys())

    def test_print_info(self):
        self.gynx.print_info()

    def test_print_size(self):
        bytes = [500, 5000, 5000000, 5000000000]
        parsed = [self.gynx.print_size(b) for b in bytes]
        self.assertEqual(parsed, ['500 B', '5.00 KB', '5.00 MB', '5.00 GB'])
