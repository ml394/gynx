''' Tests fore gynx.core.scheduler '''

from tests.base import *
from gynx.core.scheduler import GynxScheduler

def job():
    return

class TestGynxScheduler(TestCase):

    def setUp(self):
        self.scheduler = GynxScheduler(days=.00001, hours=.0001)
        self.scheduler_days = GynxScheduler(days=.00001)
        self.scheduler_hours = GynxScheduler(hours=.0001)

    def test_add_job(self):
        self.scheduler.add_job(job)

    def test_start(self):
        self.scheduler.add_job(job)
        self.scheduler.start()
        self.scheduler_days.start()
        self.scheduler_hours.start()
