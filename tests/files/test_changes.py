''' Tests for gynx.files.changes '''

from tests.base import *
from gynx.files.changes import RemoteChanges, LocalChanges
from gynx.core import Gynx
from gynx.files.operations import SyncOperations
from gynx.files.readers import RemoteFileReader
from datetime import datetime
import time


class TestRemoteChanges(TestCase):

    def setUp(self):
        setup_folders()
        self.data = Data()
        self.gynx = Gynx()
        self.test_file_name = os.path.join(
            DRIVE_DIRECTORY,
            'test_%s.txt' % str(int(datetime.now().timestamp()))
        )
        if not os.path.exists(self.test_file_name):
            with open(self.test_file_name, 'w+') as self.testfile:
                self.testfile.write(
                    '%s - This is a test file' % str(datetime.now())
                )
        self.operations = SyncOperations(
            service=self.gynx.service,
            changes=[],
            root=self.gynx.root
        )
        self.changes = RemoteChanges(gynx=self.gynx)

    def test_get(self):
        uploaded = self.operations.upload(
            mimetype = 'text/plain',
            local_path=self.test_file_name
        )
        unchanged = self.changes.get()
        time.sleep(.1)
        updated = self.gynx.service.files().update(fileId=uploaded, fields='id').execute()
        changed = self.changes.get()
        self.assertIsNotNone(changed)

    def test_watch(self):
        # self.changes.watch()
        pass

    def tearDown(self):
        cleanup_folders()


class TestLocalChanges(TestCase):

    def setUp(self):
        setup_folders()
        self.data = Data()
        self.gynx = Gynx()
        remote_files = read_remote(self.gynx)
        delete_remote_files(self.gynx.service, remote_files)
        remote_files = read_remote(self.gynx)
        path = create_local_file('file.txt')
        create_remote_file(self.gynx.service, path)
        remote_files = read_remote(self.gynx)
        self.changes = LocalChanges(
            gynx=self.gynx, rootdir=DRIVE_DIRECTORY, testing=True,
            cache=remote_files
        )

    def test_watch(self):
        self.changes.watch()
