''' Tests for gynx.files.differences '''

from tests.base import *
from gynx.files.differences import Difference, Differences


class TestDifference(TestCase):

    def setUp(self):
        self.d = Data()

    def test_dictdiffer(self):
        for diff in self.d.ALL_DIFFS:
            self.assertEqual(len(diff), 1)

    def test_str(self):
        for diff in self.d.ALL_DIFFS:
            self.assertEqual(str(diff[0]), str(diff[0].dd))

    def test_type(self):
        for diff in self.d.ALL_DIFFS:
            self.assertEqual(diff[0].type, diff[0].dd[0])
        self.assertEqual(self.d.DIFF_AB[0].type, 'change')
        self.assertEqual(self.d.DIFF_BA[0].type, 'change')
        self.assertEqual(self.d.DIFF_BC[0].type, 'add')
        self.assertEqual(self.d.DIFF_CB[0].type, 'remove')
        self.assertEqual(self.d.DIFF_CD[0].type, 'remove')
        self.assertEqual(self.d.DIFF_DC[0].type, 'add')

    def test_path(self):
        for diff in self.d.ALL_DIFFS[:2]:
            self.assertEqual(diff[0].dd[1], 'a.b.c')
            self.assertEqual(diff[0].path, './b/.')
        for diff in self.d.ALL_DIFFS[2:]:
            self.assertEqual(diff[0].dd[1], 'a.b')
            self.assertEqual(diff[0].path, 'b')
        diff_bad_type = self.d.DIFF_BROKEN_TYPE
        with self.assertRaises(TypeError):
            diff_bad_type.path
        diff_bad_path = self.d.DIFF_BROKEN_PATH
        with self.assertRaises(IOError):
            diff_bad_path.path

    def test_key(self):
        for diff in self.d.ALL_DIFFS[:2]:
            self.assertEqual(diff[0].key, 'c')
        for diff in self.d.ALL_DIFFS[2:]:
            self.assertIsNone(diff[0].key)


    def test_target(self):
        for diff in self.d.ALL_DIFFS[:2]:
            self.assertIsNone(diff[0].target)
        for diff in self.d.ALL_DIFFS[2:]:
            self.assertEqual(diff[0].target, [('d', 3)])

    def test_remote_modified(self):
        self.assertEqual(self.d.DIFF_AB[0].remote_modified, 1)
        self.assertEqual(self.d.DIFF_BA[0].remote_modified, 2)
        for diff in self.d.ALL_DIFFS[2:]:
            self.assertIsNone(diff[0].remote_modified)

    def test_local_modified(self):
        self.assertEqual(self.d.DIFF_AB[0].local_modified, 2)
        self.assertEqual(self.d.DIFF_BA[0].local_modified, 1)
        for diff in self.d.ALL_DIFFS[2:]:
            self.assertIsNone(diff[0].local_modified)

class TestDifferences(TestCase):

    def setUp(self):
        self.d = Data()

    def test_differences_with_previous(self):
        differences = Differences(self.d.DICT_A, self.d.DICT_B, previous=True)
        self.assertTrue(differences.previous)

    def test_get(self):
        for i, differences in enumerate(self.d.ALL_DIFFERENCES):
            self.assertTrue(differences_equal(
                differences.get(self.d.ALL_DICTS[i], self.d.ALL_DICTS[i+1]),
                (self.d.ALL_DIFFS[i*2], self.d.ALL_DIFFS[(i*2)+1])
            ))
        for i, files_differences in enumerate(self.d.ALL_FILES_DIFFERENCES):
            self.assertTrue(files_differences_equal(
                files_differences.get(self.d.ALL_FILES[0], self.d.ALL_FILES[i+1]),
                (self.d.ALL_FILES_DIFFS[i*2], self.d.ALL_FILES_DIFFS[(i*2)+1])
            ))

    def test_get_file_by_path(self):
        fs = {'drive': self.d.DICT_A}
        self.assertEqual(self.d.DIFFERENCES_AB.get_file_by_path('a/b/c', fs), 1)
        self.assertIsNone(self.d.DIFFERENCES_AB.get_file_by_path('a/c/b', fs))

    def test_merge_change_remote(self):
        merged_ab = self.d.FILES_DIFFERENCES_AB.merge(self.d.FILES_A, self.d.FILES_B, differences=[])
        self.assertEqual(len(merged_ab), 1)
        diff = merged_ab[0]
        self.assertEqual(diff['type'], 'change')
        self.assertFalse(diff['folder'])
        self.assertEqual(diff['local_path'], '/home/gynx/docs/file.txt')
        self.assertEqual(diff['remote_id'], 1)
        self.assertEqual(diff['target'], 'remote')
        self.assertEqual(diff['message'], 'docs/file.txt updated in local. Uploading...')

    def test_merge_change_local(self):
        merged_ac = self.d.FILES_DIFFERENCES_AC.merge(self.d.FILES_A, self.d.FILES_C, differences=[])
        self.assertEqual(len(merged_ac), 1)
        diff = merged_ac[0]
        self.assertEqual(diff['type'], 'change')
        self.assertFalse(diff['folder'])
        self.assertEqual(diff['local_path'], '/home/gynx/docs/file.txt')
        self.assertEqual(diff['remote_id'], 1)
        self.assertEqual(diff['target'], 'local')
        self.assertEqual(diff['message'], 'docs/file.txt updated in remote. Downloading...')

    def test_merge_no_changes(self):
        merged_ad = self.d.FILES_DIFFERENCES_AD.merge(self.d.FILES_A, self.d.FILES_D, differences=[])
        self.assertEqual(len(merged_ad), 0)

    def test_merge_add_remote(self):
        merged_ae = self.d.FILES_DIFFERENCES_AE.merge(self.d.FILES_A, self.d.FILES_E, differences=[])
        self.assertEqual(len(merged_ae), 1)
        diff = merged_ae[0]
        self.assertEqual(diff['type'], 'add')
        self.assertFalse(diff['folder'])
        self.assertEqual(diff['local_path'], '/home/gynx/docs/image.jpg')
        self.assertIsNone(diff['remote_id'])
        self.assertEqual(diff['target'], 'remote')
        self.assertEqual(diff['message'], 'docs/image.jpg created in local. Uploading...')

    def test_merge_remove_remote(self):
        merged_af = self.d.FILES_DIFFERENCES_AF.merge(self.d.FILES_A, self.d.FILES_F, differences=[])
        self.assertEqual(len(merged_af), 1)
        diff = merged_af[0]
        self.assertEqual(diff['type'], 'remove')
        self.assertFalse(diff['folder'])
        self.assertEqual(diff['local_path'], '/home/gynx/docs/file.txt')
        self.assertEqual(diff['remote_id'], 1)
        self.assertEqual(diff['target'], 'remote')
        self.assertEqual(diff['message'], 'docs/file.txt removed in local. Deleting remote...')

    def test_merge_add_local(self):
        merged_gd = self.d.FILES_DIFFERENCES_GD.merge(self.d.FILES_G, self.d.FILES_D, differences=[])
        self.assertEqual(len(merged_gd), 1)
        diff = merged_gd[0]
        self.assertEqual(diff['type'], 'add')
        self.assertFalse(diff['folder'])
        self.assertEqual(diff['local_path'], '/home/gynx/docs/image.jpg')
        self.assertEqual(diff['remote_id'], 3)
        self.assertEqual(diff['target'], 'local')
        self.assertEqual(diff['message'], 'docs/image.jpg created in remote. Downloading...')

    def test_merge_remove_local(self):
        merged_hd = self.d.FILES_DIFFERENCES_HD.merge(self.d.FILES_H, self.d.FILES_D, differences=[])
        self.assertEqual(len(merged_hd), 1)
        diff = merged_hd[0]
        self.assertEqual(diff['type'], 'remove')
        self.assertFalse(diff['folder'])
        self.assertEqual(diff['local_path'], '/home/gynx/docs/file.txt')
        self.assertIsNone(diff['remote_id'])
        self.assertEqual(diff['target'], 'local')
        self.assertEqual(diff['message'], 'docs/file.txt removed in remote. Deleting local...')

    def test_merge_rename_remote(self):
        merged_aj = self.d.FILES_DIFFERENCES_AJ.merge(self.d.FILES_A, self.d.FILES_J, differences=[])
        self.assertEqual(len(merged_aj), 3)
        diff = merged_aj[0]
        self.assertEqual(diff['type'], 'rename')
        self.assertFalse(diff['folder'])
        self.assertEqual(diff['local_path'], '/home/gynx/docs/renamed.txt')
        self.assertEqual(diff['new'], 'renamed.txt')
        self.assertIsNone(diff['remote_id'])
        self.assertEqual(diff['target'], 'remote')
        self.assertEqual(diff['message'], 'file.txt renamed in local to renamed.txt. Renaming remote...')

    def test_merge_rename_local(self):
        merged_id = self.d.FILES_DIFFERENCES_ID.merge(self.d.FILES_I, self.d.FILES_D, differences=[])
        self.assertEqual(len(merged_id), 3)
        diff = merged_id[0]
        self.assertEqual(diff['type'], 'rename')
        self.assertFalse(diff['folder'])
        self.assertEqual(diff['local_path'], '/home/gynx/docs/file.txt')
        #self.assertEqual(diff['new'], 'renamed.txt')
        #self.assertIsNone(diff['remote_id'])
        #self.assertEqual(diff['target'], 'local')
        #self.assertEqual(diff['message'], 'file.txt renamed in remote to renamed.txt. Renaming local...')

    def test_merge_mkdir_remote(self):
        merged_ak = self.d.FILES_DIFFERENCES_AK.merge(self.d.FILES_A, self.d.FILES_K, differences=[])
        self.assertEqual(len(merged_ak), 1)
        diff = merged_ak[0]
        self.assertEqual(diff['type'], 'add')
        self.assertTrue(diff['folder'])
        self.assertEqual(diff['local_path'], '/home/gynx/docs/folder')
        self.assertIsNone(diff['remote_id'])
        self.assertEqual(diff['target'], 'remote')
        self.assertEqual(diff['message'], 'docs/folder created in local. Creating remote directory...')

    def test_merge_mkdir_local(self):
        merged_ld = self.d.FILES_DIFFERENCES_LD.merge(self.d.FILES_L, self.d.FILES_D, differences=[])
        self.assertEqual(len(merged_ld), 1)
        diff = merged_ld[0]
        self.assertEqual(diff['type'], 'add')
        self.assertTrue(diff['folder'])
        self.assertEqual(diff['local_path'], '/home/gynx/docs/folder')
        self.assertIsNone(diff['remote_id'])
        self.assertEqual(diff['target'], 'local')
        self.assertEqual(diff['message'], 'docs/folder created in remote. Creating local directory...')

    def test_all(self):
        for fds in self.d.ALL_FILES_DIFFERENCES:
            self.assertEqual(fds.all(), fds.merge(fds.remote, fds.local, differences=[]))
            fds.print_all()
