''' Tests for gynx.files.events '''

from tests.base import *
from gynx.files.events import GynxEvent, ACTIONS
import random


class TestGynxEvent(TestCase):

    def setUp(self):
        self.events = []
        for action in ACTIONS.keys():
            self.events.append(
                GynxEvent(
                    action=action,
                    ftype=random.choice(['file', 'folder']),
                    path=DRIVE_DIRECTORY
                )
            )

    def test_str(self):
        for e in self.events:
            self.assertIn('local', str(e))
