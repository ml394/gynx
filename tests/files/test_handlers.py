''' Tests for gynx.files.events.handlers '''

from tests.base import *
from gynx.files.events import GynxEvent, ACTIONS
from gynx.files.events.handlers import GynxEventHandler
from gynx.core import Gynx


class TestGynxEventHandler(TestCase):

    def setUp(self):
        self.gynx = Gynx()
        self.handler = GynxEventHandler(
            gynx=self.gynx, rootdir=DRIVE_DIRECTORY, events=[]
        )
        self.directory = DRIVE_DIRECTORY
        i = 1
        for action in ACTIONS.keys():
            self.handler.events.append(
                GynxEvent(
                    action=action,
                    ftype=['file', 'folder'][i % 2],
                    path=DRIVE_DIRECTORY
                )
            )
            i += 1

    def test_find_event(self):
        for action in ACTIONS.keys():
            action_events = self.handler.find_event(
                {'action': action}
            )
            self.assertEqual(len(action_events), 1)
        for ftype in ['file', 'folder']:
            ftype_events = self.handler.find_event(
                {'ftype': ftype}
            )
            self.assertEqual(len(ftype_events), 2)
        path_events = self.handler.find_event(
            {'path': DRIVE_DIRECTORY}
        )
        self.assertEqual(len(path_events), 4)

    def test_read_remote(self):
        try:
            self.handler.read_remote()
        except Exception:
            failed = True
        else:
            failed = False
        self.assertFalse(failed)

    def test_get_remote_id(self):
        self.handler.cache = {'drive': {'file': {'child_id': 1}}}
        self.assertEqual(
            self.handler.get_remote_id(
                os.path.join(DRIVE_DIRECTORY, 'file')
            ),
            (1, None)
        )
        self.assertEqual(
            self.handler.get_remote_id('test'),
            (None, None)
        )
        self.assertEqual(
            self.handler.get_remote_id(
                os.path.join(DRIVE_DIRECTORY, 'test')
            ),
            (None, None)
        )


    def test_get_local_mimetype(self):
        filepath = os.path.join(
            DRIVE_DIRECTORY, 'test_mimetype.txt'
        )
        with open(filepath, 'w') as f:
            f.write('some text')
        self.assertEqual(
            self.handler.get_local_mimetype(filepath),
            'text/plain'
        )

    def test_is_lockfile(self):
        filepath = os.path.join(
            DRIVE_DIRECTORY, 'test_lockfile.txt'
        )
        self.assertFalse(self.handler.is_lockfile(filepath))
        filepath = os.path.join(
            DRIVE_DIRECTORY, 'test_lockfile.lock'
        )
        self.assertTrue(self.handler.is_lockfile(filepath))
        filepath = os.path.join(
            DRIVE_DIRECTORY, 'test_lockfile.swp'
        )
        self.assertTrue(self.handler.is_lockfile(filepath))

    def test_handle_upload(self):
        folderpath = os.path.join(DRIVE_DIRECTORY, 'test_handle_upload')
        os.mkdir(folderpath)
        filepath = os.path.join(folderpath, 'test_handle_upload.txt')
        with open(filepath, 'w') as f:
            f.write('some text')
        for ftype, path in [
            ('folder', folderpath),
            ('file', filepath)
        ]:
            event = GynxEvent(action='CREATE', ftype=ftype, path=path)
            self.handler.events.append(event)
            self.handler.handle_upload(event)
            self.assertIsNotNone(event.remote_id)

    def tearDown(self):
        self.handler.events = []
        cleanup_folders()
        delete_remote_files(
            service=self.gynx.service,
            remote=read_remote(self.gynx)
        )
