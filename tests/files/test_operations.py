''' Tests for gynx.files.operations '''

from tests.base import *
from gynx.files.operations import SyncOperations
from gynx.core import Gynx
from gynx.files.readers import LocalFileReader, RemoteFileReader
from gynx.files.differences import Differences


class TestSyncOperations(TestCase):

    def setUp(self):
        setup_folders()
        self.d = Data()
        gynx = Gynx()
        self.gynx = gynx
        self.test_file_name = os.path.join(DRIVE_DIRECTORY, 'test.txt')
        if not os.path.exists(self.test_file_name):
            with open(self.test_file_name, 'w+') as self.testfile:
                self.testfile.write('This is a test file.')
        self.test_differences = self.d.CHANGES
        self.test_operations = SyncOperations(
            service=None,
            changes=self.d.CHANGES,
            root='/home/gynx/'
        )
        local_reader = LocalFileReader(
            src=gynx.appdir,
            rootdir=DRIVE_DIRECTORY,
            quiet=False
        )
        remote_reader = RemoteFileReader(
            src=gynx.appdir,
            service=gynx.service,
            info=gynx.get_info(),
            quiet=gynx.quiet
        )
        local = local_reader.files
        remote = remote_reader.files
        self.remote = remote
        differences = Differences(
            remote_files=remote,
            local_files=local,
            previous=remote_reader.load(),
            root=DRIVE_DIRECTORY,
            initial=remote_reader.initial
        )
        self.operations = SyncOperations(
            service=gynx.service,
            changes=differences.all(),
            remote=remote,
            local=local,
            root=gynx.root,
            rf=remote_reader.remote_folders,
            testing=True
        )

    def test_get_relative_path(self):
        self.assertEqual(
            self.test_operations.get_relative_path('/home/gynx/docs/file.txt'),
            'docs/file.txt'
        )

    def test_get_file_by_path(self):
        self.assertEqual(self.test_operations.get_file_by_path('docs/file.txt', self.d.FILES_A)['size'], 5)
        self.assertEqual(self.test_operations.get_file_by_path('docs/file.txt', self.d.FILES_B)['size'], 6)
        self.assertIsNone(self.test_operations.get_file_by_path('docs/folder/file.txt', self.d.FILES_A))

    def test_create_folder(self):
        name = 'Test Folder'
        path = os.path.join(DRIVE_DIRECTORY, name)
        local_folder = self.operations.create_folder(
            name=name, target='local', path=path
        )
        self.assertIsNone(local_folder)
        self.assertTrue(os.path.isdir(path))
        remote_folder = self.operations.create_folder(
            name=name, target='remote', path=None
        )
        self.assertIsNotNone(remote_folder)
        remote_child_folder = self.operations.create_folder(
            name=name+' 2',
            target='remote',
            path=None,
            parent=remote_folder
        )
        self.assertIsNotNone(remote_child_folder)

    def test_upload(self):
        uploaded = self.operations.upload(
            mimetype='text/plain',
            local_path=self.test_file_name
        )
        self.assertIsNotNone(uploaded)
        self.assertEqual(type(uploaded), type('abc'))
        remote_folder = self.operations.create_folder(
            name='Folder', target='remote', path=None
        )
        uploaded_with_folder = self.operations.upload(
            mimetype = 'text/plain',
            local_path=self.test_file_name,
            folder=remote_folder
        )
        self.assertIsNotNone(uploaded_with_folder)
        self.operations.testing = True
        bad_upload = self.operations.upload(
            mimetype='whatever',
            local_path=''
        )
        self.assertFalse(bad_upload)
        self.operations.testing = False

    def test_download(self):
        uploaded = self.operations.upload(
            mimetype = 'text/plain',
            local_path=self.test_file_name
        )
        os.remove(self.test_file_name)
        downloaded = self.operations.download(path=self.test_file_name, file_id=uploaded)
        self.assertTrue(downloaded)

    def test_delete(self):
        filename = os.path.join(DRIVE_DIRECTORY, 'test_delete.txt')
        with open(filename, 'w') as f:
            f.write('test_delete')
        bad_filename = os.path.join(DRIVE_DIRECTORY, 'test_delete_bad.txt')
        self.assertTrue(self.operations.delete(local_path=filename))
        self.assertFalse(self.operations.delete(local_path=bad_filename))
        self.assertFalse(self.operations.delete(remote_id='nonsense'))

    def test_run(self):
        try:
            self.operations.run()
            errors = None
        except:
            errors = None
        self.assertIsNone(errors)

    def tearDown(self):
        cleanup_folders()
        delete_remote_files(self.gynx.service, self.remote)
