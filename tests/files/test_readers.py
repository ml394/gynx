''' Tests for gynx.files.readers '''

from tests.base import *
from gynx.files import readers
from gynx.core import Gynx


class TestFileReader(TestCase):

    def setUp(self):
        setup_folders()
        self.reader = readers.FileReader(src=APP_DIRECTORY)
        self.item_a = {'size': 2}
        self.item_b = {'size': 2.5}
        self.item_c = {'not_size': 2}
        self.item_d = {'size': 1}

    def test_cache(self):
        with self.assertRaises(AttributeError):
            self.reader.cache

    def test_files(self):
        with self.assertRaises(AttributeError):
            self.reader.files

    def test_json(self):
        with self.assertRaises(AttributeError):
            self.reader.json

    def test_load(self):
        with self.assertRaises(AttributeError):
            self.reader.load()

    def test_save(self):
        with self.assertRaises(AttributeError):
            self.reader.save(files=None)

    def test_size(self):
        self.assertEqual(self.reader.size(self.item_a), 2)
        self.assertEqual(self.reader.size(self.item_b), 2)
        self.assertEqual(self.reader.size(self.item_c), 0)
        self.assertEqual(self.reader.size(self.item_d), 1)

    def test_completion(self):
        items = []
        self.assertEqual(self.reader.completion(items, 0), 100)
        self.assertEqual(self.reader.completion(items, 4), 0)
        self.assertEqual(self.reader.completion(items, 10), 0)
        self.assertEqual(self.reader.completion(items, 100), 0)
        items.append(self.item_a)
        self.assertEqual(self.reader.completion(items, 4), 50)
        self.assertEqual(self.reader.completion(items, 10), 20)
        self.assertEqual(self.reader.completion(items, 100), 2)
        items.append(self.item_b)
        self.assertEqual(self.reader.completion(items, 4), 100)
        self.assertEqual(self.reader.completion(items, 10), 40)
        self.assertEqual(self.reader.completion(items, 100), 4)
        items.append(self.item_d)
        self.assertEqual(self.reader.completion(items, 4), 100)
        self.assertEqual(self.reader.completion(items, 10), 50)
        self.assertEqual(self.reader.completion(items, 100), 5)

    def tearDown(self):
        cleanup_folders()


class TestLocalFileReader(TestCase):

    def setUp(self):
        setup_folders()
        self.reader = readers.LocalFileReader(
            src=APP_DIRECTORY,
            rootdir=DRIVE_DIRECTORY
        )

    def test_cache(self):
        self.assertEqual(
            self.reader.cache,
            os.path.join(APP_DIRECTORY, 'CACHE', 'local.json')
        )

    def test_initial_load(self):
        self.assertIsNone(self.reader.load())

    def test_get(self):
        self.assertEqual(self.reader.get(), {'drive': {}})
        create_local_file('file.txt')
        self.assertEqual(len(self.reader.get()['drive'].items()), 1)
        remove_local_file('file.txt')
        self.assertEqual(self.reader.get(), {'drive': {}})

    def test_save(self):
        self.assertFalse(os.path.exists(self.reader.cache))
        self.reader.save(self.reader.get())
        self.assertTrue(os.path.exists(self.reader.cache))

    def test_load(self):
        self.assertIsNone(self.reader.load())
        self.reader.save(self.reader.get())
        self.assertIsNotNone(self.reader.load())

    def test_files(self):
        self.assertFalse(self.reader.initial)
        files = self.reader.files
        self.assertTrue(self.reader.initial)

    def test_json(self):
        self.assertEqual(type(self.reader.json), type('string'))

    def tearDown(self):
        cleanup_folders()

class TestRemoteFileReader(TestCase):

    def setUp(self):
        setup_folders()
        gynx = Gynx()
        self.reader = readers.RemoteFileReader(
            src=APP_DIRECTORY,
            service=gynx.service,
            info=gynx.get_info(),
            quiet=False
        )
        self.objects = [
            {'name': 'file.txt', 'id': 1, 'mimeType': 'txt', 'size': 1, 'modifiedTime': '2019-01-01T00:00:00+0000'},
            {'name': 'image.jpg', 'id': 2, 'parents': [4], 'mimeType': 'img', 'size': 1, 'modifiedTime': '2019-01-01T00:00:00+0000'},
            {'name': 'document.docx', 'id': 3, 'parents': [5], 'mimeType': 'doc', 'size': 1, 'modifiedTime': '2019-01-01T00:00:00+0000'},
            {'name': 'pictures', 'id': 4, 'mimeType': 'folder'},
            {'name': 'docs', 'id': 5, 'mimeType': 'folder'},
            {'name': 'screenshots', 'id': 6, 'mimeType': 'folder', 'parents': [4]}
        ]

    def test_cache(self):
        self.assertEqual(
            self.reader.cache,
            os.path.join(APP_DIRECTORY, 'CACHE', 'remote.json')
        )

    def test_initial_load(self):
        self.assertIsNone(self.reader.load())

    def test_parent(self):
        fa = {'parents': [1]}
        fb = {'parents': []}
        fc = {'not_parents': [1]}
        self.assertEqual(self.reader.parent(fa), 1)
        self.assertIsNone(self.reader.parent(fb))
        self.assertIsNone(self.reader.parent(fc))

    def test_get_object(self):
        for i in range(3):
            self.assertEqual(
                self.reader.get_object(i+1, self.objects),
                self.objects[i]
            )

    def test_get_path(self):
        self.assertEqual(self.reader.get_path(self.objects[0], self.objects), '/file.txt')
        self.assertEqual(self.reader.get_path(self.objects[1], self.objects), '/pictures/image.jpg')
        self.assertEqual(self.reader.get_path(self.objects[2], self.objects), '/docs/document.docx')

    def test_fetch(self):
        fetched = self.reader.fetch()
        self.assertEqual(type(fetched), type([]))
        # TODO: add more meaningful tests

    def test_index_files(self):
        indexed = self.reader.index_files(self.objects)
        self.assertEqual(len(indexed.keys()), 1)
        self.assertIn('drive', indexed.keys())
        # TODO: add more meaningful tests

    def test_get(self):
        files = self.reader.get()
        self.assertEqual(type(files), type({}))
        # TODO: add more meaningful tests

    def tearDown(self):
        cleanup_folders()
