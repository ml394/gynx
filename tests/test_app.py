''' Tests for gynx.app '''

from tests.base import *

# Test main import
def test_import():
    try:
        import gynx
        imported = True
    except ImportError:
        imported = False
    assert(imported)

from gynx.app import GynxApp, main

def test_main():
    try:
        main()
    except:
        valid = True
    else:
        valid = True
    assert(valid)

class TestGynxApp(TestCase):

    def setUp(self):
        self.app = GynxApp(testing=True)

    def test_verbose(self):
        self.assertTrue(self.app.verbose)

    def test_dry_run(self):
        self.assertFalse(self.app.dry_run)
        app_dry = GynxApp(dry_run=True, testing=True)
        self.assertTrue(app_dry.dry_run)
        try:
            app_dry.execute()
        except:
            complete = True
        else:
            complete = True
        self.assertTrue(complete)

    def test_schedule(self):
        self.assertFalse(self.app.schedule)
        app_schedule = GynxApp(schedule=.0001, testing=True)
        self.assertTrue(app_schedule.schedule)
        try:
            app_schedule.execute()
        except:
            complete = True
        else:
            complete = True
        self.assertTrue(complete)

    def test_duration(self):
        self.assertIsNone(self.app.duration)

    def test_start(self):
        self.assertFalse(self.app.start)
        app_start = GynxApp(start=True, testing=True)
        self.assertTrue(app_start.start)
        try:
            app_start.execute()
        except:
            complete = True
        else:
            complete = True
        self.assertTrue(complete)

    def test_run(self):
        try:
            self.app.run()
        except:
            complete = True
        else:
            complete = True
        self.assertTrue(complete)

    def test_execute(self):
        try:
            self.app.execute()
        except:
            complete = True
        else:
            complete = True
        self.assertTrue(complete)
